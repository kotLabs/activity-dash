#!/bin/bash

echo Starting Gunicorn
exec gunicorn -b 0.0.0.0:8000 --workers 3 app:server