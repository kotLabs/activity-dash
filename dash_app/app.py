import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
from dash.dependencies import Input, Output
import plotly.graph_objects as go
from plotly.express.colors import qualitative
import pandas as pd
import numpy as np

app = dash.Dash(__name__,
                url_base_pathname='/dash/',
                meta_tags=[{"name": "viewport", "content": "width=device-width, initial-scale=1"}],
                )

server = app.server

app_color = {"graph_bg": "#082255", "graph_line": "#007ACE"}

# Load data
dateCols = ['date']
df = pd.read_csv('activity_data.csv', parse_dates=dateCols, infer_datetime_format=True)
df['duration'] = pd.to_timedelta(df['duration'])

# Define options for dropdown list
dropdown_options = [{'label': x, 'value': x} for x in np.sort(df['year'].unique())[::-1]]

# Create dict for full month names
months_names = {'1': 'January', '2': 'February', '3': 'March', '4': 'April', '5': 'May', '6': 'June',
                '7': 'July', '8': 'August', '9': 'September', '10': 'October', '11': 'November', '12': 'December'}

# Define colors for activities. Warning: solution allows max 24 activities.
activity_colors = {}
i = 0
for activityName in df.activityName.unique():
    activity_colors[activityName] = qualitative.Light24[i]
    i += 1

# App Layout
app.layout = html.Div(
    [
        # Header
        html.Div(
            [
                html.Div(
                    [
                        html.H4("ACTIVITY DASH", className="app__header__title"),
                        html.P(
                            "This app visualizes data of my training sessions recorded by GPS activity tracker"
                            " during last few years",
                            className="app__header__title--grey",
                        ),
                        html.P(
                            "I recommend selecting year 2017, as it has the most diverse data.",
                            className="app__header__text"
                        ),
                        html.P(
                            "Hover mouse over graph to update data in other elements.",
                            className="app__header__text"
                        )
                    ],
                    className="app__header__desc",
                ),
            ],
            className="app_header",
        ),
        html.Div(
            [

                html.Div(
                    [
                        # Main graph
                        html.Div(
                            [
                                html.Div(
                                    [
                                        html.H6('Select year: ',
                                                className="three columns u-pull-left"),
                                        dcc.Dropdown(
                                            id='dropdown-year',
                                            options=dropdown_options,
                                            value=df['year'].max(),
                                            searchable=False,
                                            clearable=False,
                                            className="one-third column u-pull-left dropdown__theme-colors",
                                        ),
                                    ],
                                    className="main-graph-container__header"
                                ),
                                dcc.Graph(
                                    id='main-graph',
                                    figure=dict(
                                        layout=dict(
                                            plot_bgcolor=app_color["graph_bg"],
                                            paper_bgcolor=app_color["graph_bg"],
                                        )
                                    ),
                                ),
                            ],
                            className="main-graph-container"
                        ),
                        # Summary tables
                        html.Div(
                            [
                                html.Div(
                                    [
                                        dash_table.DataTable(
                                            id='table-yearly',
                                            style_as_list_view=True,
                                            style_cell={
                                                'backgroundColor': app_color["graph_bg"]
                                            },
                                            style_cell_conditional=[
                                                {'if': {'column_id': 'column0'},
                                                 'textAlign': 'left'
                                                 }
                                            ],
                                            style_header={
                                                'fontWeight': 'bold'
                                            },
                                        ),
                                    ],
                                    className="one-half column table-container"
                                ),
                                html.Div(
                                    [
                                        dash_table.DataTable(
                                            id='table-monthly',
                                            style_as_list_view=True,
                                            style_cell={
                                                'backgroundColor': app_color["graph_bg"]
                                            },
                                            style_cell_conditional=[
                                                {'if': {'column_id': 'column0'},
                                                 'textAlign': 'left'
                                                 }
                                            ],
                                            style_header={
                                                'fontWeight': 'bold',
                                            },
                                        ),
                                    ],
                                    className=" one-half column table-container"
                                ),
                            ],
                            className="table-containers-wrapper"
                        ),
                    ],
                    className="eight columns"
                ),

                # Right side panel
                html.Div(
                    [
                        dcc.Graph(
                            id='distance-graph',
                            figure=dict(
                                layout=dict(
                                    plot_bgcolor=app_color["graph_bg"],
                                    paper_bgcolor=app_color["graph_bg"],
                                )
                            ),
                            className="distance-graph-container"
                        ),
                    ],
                    className="four columns right-panel u-pull-right",
                )
            ],
            className="app__content"
        ),
    ],
    className="app__container"
)


def timedeltas_to_str(df_timedeltas):
    """ Helper function to print timedeltas without 'x days..'
    :returns Pandas Series"""
    comps = df_timedeltas.dt.components
    return comps.apply(lambda row: "{}:{:02d}:{:02d}".format(row.days * 24 + row.hours, row.minutes, row.seconds),
                       axis=1)


def gen_table_data_attr(df_table):
    """ Helper function to generate datatable data attribute"""
    data = []
    dur_max = df_table.duration.max()
    dur_str = "{}:{:02d}:{:02d}".format(int(dur_max.total_seconds() / 3600),
                                        int(dur_max.total_seconds() / 60 % 60),
                                        int(dur_max.total_seconds() % 60))
    data.append({'column0': 'Longest distance',
                 'value': '{} km'.format(np.round(df_table.distance.max() / 1000, 2)),
                 'date': df_table[df_table.distance == df_table.distance.max()].date.dt.date})
    data.append({'column0': 'Longest session',
                 'value': dur_str,
                 'date': df_table[df_table.duration == df_table.duration.max()].date.dt.date})
    data.append({'column0': 'Sessions counted',
                 'value': len(df_table.index),
                 'date': '-'})
    return data


@app.callback(
    Output('main-graph', 'figure'),
    [Input('dropdown-year', 'value')])
def update_main_graph(year):
    df_y = df[df.year == year]
    data = []
    for activity in df_y.activityName.unique():
        df_y_n = df_y[df_y.activityName == activity]
        distances = df_y_n.distance.apply(lambda x: np.round(x / 1000, 3))
        durations = timedeltas_to_str(df_y_n.duration)
        trace = go.Bar(
            x=df_y_n['month'],
            y=distances,
            width=0.8,
            marker=dict(
                color=activity_colors[activity],
                line=dict(
                    width=1,
                    color='#7fafdf'
                )
            ),
            hovertemplate="Distance: %{y} km" +
                          "<br>%{text}",
            text=["Duration: {}".format(x) for x in durations],
            name=activity,
            customdata=df_y_n['activityName'])
        data.append(trace)
    return {'data': data,
            'layout': go.Layout(
                plot_bgcolor=app_color["graph_bg"],
                paper_bgcolor=app_color["graph_bg"],
                font={"color": "#7fafdf"},
                yaxis=dict(
                    title='Distance, km'
                ),
                xaxis=dict(
                    tickmode='array',
                    tickvals=[x for x in range(1, 13)],
                    ticktext=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                ),
                xaxis_tickangle=-45,
                barmode='stack',
                hovermode='closest')
            }


@app.callback(Output('distance-graph', 'figure'),
              [Input('dropdown-year', 'value'),
               Input('main-graph', 'hoverData')])
def update_distance_graph(year, hoverdata):
    labels = []
    values = []
    pull = []
    pulled = hoverdata['points'][0]['customdata'] if hoverdata is not None else ""
    df_filtered = df.query('year==@year')
    colors = []
    for activity in df_filtered.activityName.unique():
        labels.append(activity)
        values.append(np.round(df_filtered.query('activityName==@activity').distance.sum() / 1000, 1))
        pull.append(0.3 if activity == pulled else 0)
        colors.append(activity_colors[activity])

    return {'data': [go.Pie(
        labels=labels,
        values=values,
        pull=pull,
        marker=dict(colors=colors),
        hole=0.2,
        showlegend=False,
        textinfo='percent+label'
    )], 'layout': go.Layout(
        title_text="Tracked distances of {}".format(year),
        plot_bgcolor=app_color["graph_bg"],
        paper_bgcolor=app_color["graph_bg"],
        font={"color": "#7fafdf"},
    )
    }


@app.callback([Output('table-yearly', 'data'),
               Output('table-yearly', 'columns')],
              [Input('dropdown-year', 'value')])
def gen_table_yearly(year):
    columns = [{'name': "{} yearly".format(str(year)), 'id': 'column0'},
               {'name': 'Value', 'id': 'value'},
               {'name': 'Date', 'id': 'date'}]

    data = gen_table_data_attr(df.query('year==@year'))

    return data, columns


@app.callback([Output('table-monthly', 'data'),
               Output('table-monthly', 'columns')],
              [Input('dropdown-year', 'value'),
               Input('main-graph', 'hoverData')])
def gen_table_monthly(year, hoverdata):
    if hoverdata is None:
        month = df.query('year==@year').month.min()
    else:
        month = hoverdata['points'][0]['label']

    columns = [{'name': "{} {}".format(year, months_names[str(month)]), 'id': 'column0'},
               {'name': 'Value', 'id': 'value'},
               {'name': 'Date', 'id': 'date'}]

    data = gen_table_data_attr(df.query('year==@year & month==@month'))

    return data, columns


if __name__ == '__main__':
    app.run_server(debug=True)
