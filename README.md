**Running**

Building Docker container:

`cd dash_app`

`docker build -t registry.gitlab.com/kotlabs/activity-dash/dash-app:latest .
`

Running stand-alone container:
`docker container run -p 8000:8000 registry.gitlab.com/kotlabs/activity-dash/dash-app
`

App will be served at http://0.0.0.0:8000/dash/


Running docker stack with nginx:
`docker stack deploy -c dash-stack.yaml dash
`

App will be served at http://0.0.0.0:8080/dash/

**Data processing**

Data processing was done in Jupyter notebook data-processing.ipynb. However, original raw data exported from Polar account wasn't
included in repository, as it contains a lot of personal info.